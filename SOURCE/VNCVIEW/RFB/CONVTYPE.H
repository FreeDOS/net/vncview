#define Bool unsigned char

#define CARD8 unsigned char
#define CARD16 unsigned int
#define CARD32 unsigned long

#define True 1
#define False 0

#define String char *
#define strncasecmp strnicmp

